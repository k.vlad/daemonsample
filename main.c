#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <ucontext.h>
#include <execinfo.h>
#include <errno.h>


#define CHILD_NEED_WORK         1
#define CHILD_NEED_TERMINATE    2


static int startWorkingThread(void)
{
    int status = 0;
    // external linked code here
    return status;
}

static void deleteWorkingThread(void)
{
    // ...
}

static void signal_error(int sig, siginfo_t *si, void *ptr)
{
    void    *error_addr;
    void    *trace[16];

    int     trace_size;

    char  **msg;

    printf("Signal %s on addr %x\n", strsignal(sig), si->si_addr);

    error_addr = (void*)((ucontext_t*)ptr)->uc_mcontext.fault_address; // for armhf should be this

    trace_size = backtrace(trace, 16);
    trace[1] = error_addr;

    msg = backtrace_symbols(trace, trace_size);
    if (msg != NULL)
    {
        for (int i = 1; i < trace_size; i++)
        {
            printf("%s\n", msg[i]);
        }
        free(msg);
    }
    puts("Daemon stopped");
    return CHILD_NEED_WORK;
}

static int process(void)
{
    struct sigaction    sa;
    sigset_t            sigset;
    int                 signo, status;

    sa.sa_flags = SA_SIGINFO; // verbose information
    sa.sa_sigaction = signal_error;

    sigemptyset(&sa.sa_mask);

    sigaction(SIGFPE, &sa, 0); // FPU error
    sigaction(SIGILL, &sa, 0); // instruction error
    sigaction(SIGSEGV, &sa, 0); // memory error
    sigaction(SIGBUS, &sa, 0); // bus error

    sigemptyset(&sigset);
    sigaddset(&sigset, SIGQUIT); // add quit signal
    sigaddset(&sigset, SIGINT);  // add stop signal
    sigaddset(&sigset, SIGTERM); // add termination signal
    sigaddset(&sigset, SIGUSR1); // user signal
    sigprocmask(SIG_BLOCK, &sigset, NULL);

    puts("Daemon started");

    status = startWorkingThread();

    if (!status)
    {
        for (;;)
        {
            sigwait(&sigset, &signo); // wait for some signal

            if (signo == SIGUSR1)
            {
                // Just do something
            }
            else
            {
                break;
            }
        }
        deleteWorkingThread();
    }
    else
    {
        puts("Error in thread creation");
    }
    puts("Daemon stopped");
    return CHILD_NEED_TERMINATE;
}

static int monitoring(void)
{
    pid_t       pid;
    int         status;
    bool        start_needed = true;

    sigset_t    sigset;
    siginfo_t   siginfo;

    sigemptyset(&sigset); // initialize signalmask to empty
    sigaddset(&sigset, SIGINT); // add stop signal
    sigaddset(&sigset, SIGTERM); // add termination signal
    sigaddset(&sigset, SIGCHLD); // add change child's status signal
    sigaddset(&sigset, SIGUSR1); // user signal
    sigprocmask(SIG_BLOCK, &sigset, NULL);

    for(;;)
    {
        if (start_needed) { pid = fork();}

        start_needed = true;

        switch (pid)
        {
            case -1: // error
            {
                printf("%s\n", strerror(errno));
            }
            break;
            case 0: // child
            {
                status = process();
                exit(status);
            }
            break;
            default: // parent - should be > 0
            {
                sigwaitinfo(&sigset, &siginfo);

                if (siginfo.si_signo == SIGCHLD) // arrive signal from child
                {
                    wait(&status);
                    status = WEXITSTATUS(status);
                    if (status == CHILD_NEED_TERMINATE)
                    {
                        puts("Child stopped");
                        break;
                    }
                    else if (status == CHILD_NEED_WORK)
                    {
                        puts("child restart");
                    }
                }
                else // other signal
                {
                    printf("Signal %s\n", strsignal(siginfo.si_signo));
                    kill(pid, SIGTERM);
                    status = 0;
                    break;
                }
            }
            break;
        }
    }

    puts("Stop");
    return status;
}

int main(int argc, char **argv)
{
    pid_t   pid;
    int     status;

    pid = fork();
    switch (pid)
    {
        case -1: // error
        {
            puts("Daemon is not started");
            return -1;
        }
        break;
        case 0: // child
        {
            umask(0); // set permissions
            setsid(); // set leader session, now it's independent from parent
            status = monitoring();
            return status;
        }
        break;
        default: return 0; // parent
        break;
    }
}
